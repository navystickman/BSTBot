from typing import Optional
import discord as dc
from discord.ext import bridge, commands
from cogs.proposallimit.proposaldailylimit import getLimitForMember
from utils.cogs import Cog
from utils.stores import ConfigStore, MiscStore
from utils._funcs import log, logToDiscord, getChannel
from utils import error_handling
from utils.ids import Channels, Roles
from utils.const import OWNER_ID


def setup(BOT):
	BOT.add_cog(ProposalLimitCommands(BOT))


async def addRulesMadeCommand(ctx: bridge.BridgeExtContext, member: dc.Member, amount: int, prefix: str = ""):
    limit = getLimitForMember(member)
    with MiscStore() as miscStore:
        if member.id not in miscStore.rulesMadeToday:
            await ctx.respond(allowed_mentions=dc.AllowedMentions.none(), content=f':skull: <@{member.id}> has not proposed any rules yet!')
        else:
            miscStore.rulesMadeToday[member.id] += amount
            rulesMade = miscStore.rulesMadeToday[member.id]
            mention = 'You'
            if member.id != ctx.author.id:
                mention = f'<@{member.id}>'
            await ctx.respond(allowed_mentions=dc.AllowedMentions.none(), content=f'{prefix} {mention} can now submit `{limit - rulesMade}` more rules today!')


class ProposalLimitCommands(Cog):

    @bridge.bridge_command()
    @commands.check(error_handling.is_admin)
    async def refund(self, ctx: bridge.BridgeExtContext, member: dc.Member, amount: int = 1):
        await addRulesMadeCommand(ctx, member, -amount)

    @bridge.bridge_command()
    @commands.check(error_handling.is_admin)
    async def unrefund(self, ctx: bridge.BridgeExtContext, member: dc.Member, amount: int = 1):
        await addRulesMadeCommand(ctx, member, amount)
    
    @bridge.bridge_command()
    @commands.check(error_handling.in_bot_commands)
    async def rulesleft(self, ctx: bridge.context.BridgeExtContext, member: Optional[dc.Member] = None):
        assert isinstance(ctx.author, dc.Member)
        you = member is None
        if you:
            member = ctx.author
        
        limit = getLimitForMember(member)
        rulesLeft = ""
        if member.id == OWNER_ID:
            rulesLeft = "a lot of"
        else:
            with MiscStore(True) as miscStore:
                if member.id not in miscStore.rulesMadeToday:
                    if you:
                        await ctx.respond(allowed_mentions=dc.AllowedMentions.none(), content=f'You haven\'t proposed any rules yet!')
                    else:
                        await ctx.respond(allowed_mentions=dc.AllowedMentions.none(), content=f'<@{member.id}> has not proposed any rules yet!')
                    return
                rulesLeft = limit - miscStore.rulesMadeToday[member.id]
        
        if you:
            await ctx.respond(allowed_mentions=dc.AllowedMentions.none(), content=f'You have `{rulesLeft}` rules left to propose today!')
        else:
            await ctx.respond(allowed_mentions=dc.AllowedMentions.none(), content=f'<@{member.id}> has `{rulesLeft}` rules left to propose today!')
