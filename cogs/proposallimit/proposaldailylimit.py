import discord as dc
from discord.ext import tasks, commands
from cogs.rulemaking._util import RULE_MAKING_CHANNELS
from utils.cogs import Cog
from utils.stores import ConfigStore, MiscStore
from utils._funcs import log, logToDiscord, getChannel
from utils.ids import Channels, Roles
from utils.const import NEWDAYTIME


def setup(BOT):
	BOT.add_cog(ProposalDailyLimit(BOT))


def getLimitForMember(member: dc.Member):
    limit = ProposalDailyLimit.BASE_DAILY_LIMIT
    if member.get_role(Roles.vip):
        limit = 7
    return limit


class ProposalDailyLimit(Cog):


    BASE_DAILY_LIMIT = 5
    ACTIVE_CHANNELS = ( Channels.ruleMaking, )


    @commands.Cog.listener()
    async def on_message(self, message: dc.Message):
        if message.channel.id not in self.ACTIVE_CHANNELS:
            return
        
        assert isinstance(message.author, dc.Member)
        if message.author.bot or message.author.get_role(Roles.admin):
            return
        
        with MiscStore() as miscStore:
            limit = getLimitForMember(message.author)

            rulesMadeToday = 0
            if message.author.id in miscStore.rulesMadeToday:
                rulesMadeToday = miscStore.rulesMadeToday[message.author.id]
            
            if rulesMadeToday < limit:
                miscStore.rulesMadeToday[message.author.id] = rulesMadeToday + 1
            else:
                alertContent = f"<@{message.author.id}> you have already submitted your daily limit of {rulesMadeToday} rules <#{message.channel.id}> today!\nHere's your message:\n>>> {message.content}"
                ruleDiscussion = getChannel(Channels.ruleDiscussion, self.BOT)
                await ruleDiscussion.send(alertContent)
                self.BOT.last_deleted_message = message.id
                await message.delete()

    @commands.Cog.listener()
    async def on_ready(self):
        self.every_day.start()

    @tasks.loop(time=NEWDAYTIME)
    async def every_day(self):
        with ConfigStore(True) as config:
            with MiscStore() as miscStore:
                await logToDiscord(self.BOT, 'Daily Reset', f'Deleted {len(miscStore.rulesMadeToday.keys())} entries')
                miscStore.rulesMadeToday = {}
