import discord as dc
from discord.ext import tasks, commands
from utils.cogs import Cog
from utils._funcs import getChannel
from utils.ids import Channels
from utils.const import NEWDAYTIME


def setup(BOT):
	BOT.add_cog(DailyBotNotification(BOT))


class DailyBotNotification(Cog):

    CHANNEL = Channels.botCommands

    @commands.Cog.listener()
    async def on_ready(self):
        self.every_day.start()

    @tasks.loop(time=NEWDAYTIME)
    async def every_day(self):
        await getChannel(self.CHANNEL, self.BOT).send(content="(daily notification) hi guys everyone's proposals reset (also everyone got 1 coin)")
