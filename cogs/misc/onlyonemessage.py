import discord as dc
from discord.ext import commands
from utils.cogs import Cog
from utils.ids import Roles, Channels
from utils.stores import MiscStore


def setup(BOT):
	BOT.add_cog(OnlyOneMessage(BOT))


class OnlyOneMessage(Cog):

    ACTIVE_CHANNEL = Channels.onlyOneMessage

    @commands.Cog.listener()
    async def on_message(self, message: dc.Message):
        if message.channel.id != self.ACTIVE_CHANNEL or message.author.bot:
            return
        with MiscStore() as miscStore:
            if message.author.id in miscStore.onlyOneMessage:
                await message.delete()
            else:
                miscStore.onlyOneMessage.append(message.author.id)
