from typing import Optional
import discord as dc
from discord.ext import commands
from utils.cogs import Cog
from utils.ids import Roles, Channels
from utils._funcs import getRole, getMessage
from utils import error_handling
import re


def setup(BOT):
	BOT.add_cog(BigSmallGeneral(BOT))


class BigSmallGeneral(Cog):

    ROLE = Roles.nationalHero
    BIG_GENERAL = ( Channels.bigGeneral, )
    SMALL_GENERAL = ( Channels.smallGeneral, )
    ACTIVE_CHANNELS = ( *BIG_GENERAL, *SMALL_GENERAL )

    @commands.Cog.listener()
    async def on_message(self, message: dc.Message):
        if message.author.bot or message.channel.id not in self.ACTIVE_CHANNELS:
            return
        await self.checkMessage(message)

    @commands.Cog.listener()
    async def on_raw_message_edit(self, payload: dc.RawMessageUpdateEvent):
        if payload.channel_id not in self.ACTIVE_CHANNELS:
            return
        
        message = await getMessage(payload.message_id, payload.channel_id, self.BOT)

        if message.author.bot:
            return
        
        await self.checkMessage(message)

    async def checkMessage(self, message: dc.Message, text: Optional[str] = None):
        assert isinstance(message.author, dc.Member)
        if text is None:
            text = message.content
        if message.channel.id in self.BIG_GENERAL:
            filtered = re.sub(pattern=r"^# .*", repl="", string=text, flags=re.RegexFlag.MULTILINE)
            filtered = re.sub(pattern=r"^[^a-zA-Z0-9\-:;,.!?#&%$@^]*", repl="", string=filtered, flags=re.RegexFlag.MULTILINE)
            filtered = re.sub(pattern=r"https:\/\/.*?\/.*?\.....?", repl="", string=filtered)
            filtered = re.sub(pattern=r"https:\/\/tenor\.com\/view\/.*?-gif-[0-9]*", repl="", string=filtered)
            filtered = filtered.strip()
            if filtered != "":
                if message.author.get_role(Roles.shameRole):
                    channel = message.channel
                    await message.delete()
                    await channel.send(content=f"# <@{message.author.id}> No", delete_after=1)
                else:
                    await message.author.add_roles(getRole(Roles.shameRole, message.guild))
                    if re.sub(pattern=r"^## .*?$", repl="", string=text) == "":
                        await message.channel.send(reference=message, content=f"# <@{message.author.id}> that's medium, not big. you now have <@&{Roles.shameRole}>")
                    else:
                        await message.channel.send(reference=message, content=f"# <@{message.author.id}> your message has non-big text. you now have <@&{Roles.shameRole}>")
        
        elif message.channel.id in self.SMALL_GENERAL:
            filtered = re.sub(pattern=r"(?:^# )|(?:^## )", repl="", string=text, flags=re.RegexFlag.MULTILINE)
            if filtered != text:
                channel = message.channel
                await message.delete()
                await channel.send(content=f"<@{message.author.id}> Only small messages allowed in <#{channel.id}>", delete_after=3)
