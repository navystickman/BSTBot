import discord as dc
from discord.utils import get
from discord.ext import bridge, commands
from utils.stores import MiscStore
from utils.cogs import Cog
from utils.ids import Roles
from utils._funcs import getRole
from utils import error_handling
from random import random


def setup(BOT):
	BOT.add_cog(CMDReminders(BOT))


class CMDReminders(Cog):

    ROLE = Roles.shameRole

    @bridge.bridge_command()
    @commands.check(error_handling.in_bot_commands)
    async def noreminders(self, ctx: bridge.context.BridgeExtContext):
        assert isinstance(ctx.author, dc.Member)
        if ctx.author.get_role(self.ROLE):
            await ctx.reply(":skull: Smh you already have the No Reminders role!")
            return
        with MiscStore() as miscStore:
            if random() < miscStore.reminderChance:
                await ctx.author.add_roles(getRole(self.ROLE, ctx.guild))

                miscStore.reminderChance *= 0.75
                await ctx.reply(f"Gave you the No Reminders role! The success chance for this command is now `{round(miscStore.reminderChance*100*100)/100}%`!")
                return
            await ctx.reply(f"Command failed. Try again for a `{round(miscStore.reminderChance*100*100)/100}%` chance to have reminders disabled")

    @bridge.bridge_command()
    @commands.check(error_handling.in_bot_commands)
    async def yesreminders(self, ctx: bridge.context.BridgeExtContext):
        assert isinstance(ctx.author, dc.Member)
        if ctx.author.get_role(self.ROLE):
            await ctx.author.remove_roles(getRole(self.ROLE, ctx.guild))

            with MiscStore(True) as miscStore:
                await ctx.reply(f"Took away your No Reminders role! The success chance for `-noreminders` is still `{round(miscStore.reminderChance*100*100)/100}%`")
        else:
            await ctx.reply(":skull: You do not have the No Reminders role")

    @bridge.bridge_command()
    @commands.check(error_handling.is_admin)
    async def resetreminders(self, ctx: bridge.context.BridgeExtContext):
        with MiscStore() as miscStore:
            miscStore.reminderChance = 1
        await ctx.reply(f"Reset the success chance for `-noreminders` back to 100%")
