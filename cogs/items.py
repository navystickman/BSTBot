import discord as dc
from discord.ext import bridge, commands
from cogs.proposallimit.commands import addRulesMadeCommand
from utils.cogs import Cog
from utils.ids import Channels
from utils._funcs import logToDiscord
from utils import error_handling


def setup(BOT):
	BOT.add_cog(Items(BOT))


class Items(Cog):

    @bridge.bridge_group(invoke_without_command=True)
    @commands.check(error_handling.in_bot_commands)
    async def item(self, ctx: bridge.BridgeExtContext):
        await error_handling.availableSubcommands(
            ctx,
            self.item,
            message="Here's a list of items you can use with -item:",
            separator="\n",
            subcommandFormat=lambda s: f'`{s.name}` ({s.extras["PRICE"]} coins) - {s.description}',
        )

    @item.command(
        extras={"PRICE": 40},
        description=f"Get an extra rule proposal in <#{Channels.ruleMaking}> for the day.",
        help=f"You can submit an extra proposal in <#{Channels.ruleMaking}> regardless of the time-based limit on submitting proposals."
    )
    async def timebypass(self, ctx: bridge.BridgeExtContext):
        assert ctx.command is not None
        PRICE = ctx.command.extras["PRICE"]
        await error_handling.bal_purchase(ctx, PRICE, "a Time Bypass")
        await logToDiscord(self.BOT, "Item Purchase", f"<@{ctx.author.id}> purchased Time Bypass for {PRICE}")

        assert isinstance(ctx.author, dc.Member)
        await addRulesMadeCommand(ctx, ctx.author, -1, "Successfully purchased a Time Bypass!")
