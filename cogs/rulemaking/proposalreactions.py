import discord as dc
from discord.ext import commands
from cogs.rulemaking._util import RULE_MAKING_CHANNELS, VOTE_REACTIONS, REACTION_EMOTES, LOL
from utils.cogs import Cog
from utils.stores import ConfigStore
from utils._funcs import zwsp, log, logToDiscord, emojiName, getMessage
from utils.ids import Roles


def setup(BOT):
	BOT.add_cog(ProposalReactions(BOT))

class ProposalReactions(Cog):

    ACTIVE_CHANNELS = ( *RULE_MAKING_CHANNELS, )
    ALLOWED_REACTIONS = ( *VOTE_REACTIONS, *REACTION_EMOTES )

    @commands.Cog.listener()
    async def on_message(self, message: dc.Message):
        if message.channel.id not in self.ACTIVE_CHANNELS or not isinstance(message.channel, dc.TextChannel):
            return
        
        if message.author.bot and not (self.BOT.user and message.author.id == self.BOT.user.id and message.content[0] == zwsp):
            return
        
        await self.addVotingReactions(message)

    
    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload: dc.RawReactionActionEvent):
        if payload.channel_id not in self.ACTIVE_CHANNELS or (self.BOT.user and payload.user_id == self.BOT.user.id) or payload.member is None:
            return

        emoji = emojiName(payload.emoji)
        message = await getMessage(payload.message_id, payload.channel_id, self.BOT)

        if payload.message_id < 1137018933971521556:
            await message.remove_reaction(payload.emoji, payload.member)
            return

        if emoji == LOL:
            await self.addVotingReactions(message)
        
        if emoji not in self.ALLOWED_REACTIONS and payload.member.get_role(Roles.admin) is None:
            nonBotReaction = False
            for reaction in message.reactions:
                if emojiName(reaction.emoji) != emojiName(payload.emoji):
                    continue
                async for user in reaction.users():
                    if not user.bot and not user.id == payload.user_id:
                        nonBotReaction = True
            if not nonBotReaction:
                await message.remove_reaction(payload.emoji, payload.member)


    async def addVotingReactions(self, message: dc.Message):
        try:
            await message.add_reaction('👍')
            await message.add_reaction('👎')
        except dc.errors.NotFound:
            pass
        except dc.errors.Forbidden:
            self.BOT.last_deleted_message = message.id
            await message.delete()
            await message.channel.send(content=f"<@{message.author.id}> Unblock Imposter to use <#{message.channel.id}>!", delete_after=7)
