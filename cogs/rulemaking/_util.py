from utils.ids import Channels

LOL = "🙂"
RULE_MAKING_CHANNELS = ( Channels.ruleMaking, Channels.itemCrafting, Channels.twistMaking )
EMOTE_PENDING = "⏲️"
EMOTE_ACCEPTED = "✅"
EMOTE_CANCELLED = "❌"
PLUS_REACTIONS = ( "👍", )
MINUS_REACTIONS = ( "👎", )
VOTE_REACTIONS = ( *PLUS_REACTIONS, *MINUS_REACTIONS )
EMOTES_VETO = ( "veto:1136591247637749760", )
EMOTES_INSTA_APPROVE = ( "ThanosWhale:1136592393660026941", )
EMOTES_FINALIZED = ( EMOTE_ACCEPTED, *EMOTES_VETO, *EMOTES_INSTA_APPROVE, "⚓" )
REACTION_EMOTES = ( "🖕", "❔", "🤔", "😁", "🥺", "🤯", "😂", "trol:1136590805872693308", "tallyhall:1137741760227979416" )

EMOTE_ONEROUND = "🔂"

def getProposalShorthand(proposalText: str):
    MAXLENGTH = 20
    MINLENGTH = 5
    s = ""
    words = proposalText.split(' ')
    ellipsis = False
    for word in words:
        if len(s) + len(word) < MAXLENGTH:
            s += word + " "
        else:
            if len(s) < MINLENGTH:
                word = word.rstrip(".,!?+=-~\"';:[](){}\\/*_|`")
                s += word
                s = s[:MAXLENGTH - 1]
            ellipsis = True
            break
    s = s.strip()
    if ellipsis:
        s = s.rstrip(".,!?+=-~\"';:[](){}\\/*_|`")
        s += "…"
    return s
