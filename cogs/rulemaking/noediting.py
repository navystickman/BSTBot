import discord as dc
from discord.ext import commands
from cogs.rulemaking._util import RULE_MAKING_CHANNELS
from utils.cogs import Cog
from utils._funcs import log, getChannel, getMessage
from utils.ids import Channels


def setup(BOT):
	BOT.add_cog(NoEditing(BOT))

class NoEditing(Cog):

    ACTIVE_CHANNELS = ( *RULE_MAKING_CHANNELS, )

    @commands.Cog.listener()
    async def on_raw_message_edit(self, payload: dc.RawMessageUpdateEvent):
        if payload.channel_id not in self.ACTIVE_CHANNELS or payload.message_id < 1137018933971521556:
            return

        message = await getMessage(payload.message_id, payload.channel_id, self.BOT)
        if (message.edited_at is None) or (payload.cached_message and payload.cached_message.content == message.content):
            return

        alertContent = f"<@{message.author.id}> you may not edit messages in <#{payload.channel_id}> (check rule-making pins)!\nFeel free to re-send your rule. Your message was:\n>>> {message.content}"
        ruleDiscussion = getChannel(Channels.ruleDiscussion, self.BOT)
        await ruleDiscussion.send(alertContent)

        self.BOT.last_deleted_message = message.id
        await message.delete()
