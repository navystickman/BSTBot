import discord as dc
from discord.ext import commands
from cogs.rulemaking._util import RULE_MAKING_CHANNELS, getProposalShorthand
from cogs.rulemaking import _util
from utils.cogs import Cog
from utils.stores import ConfigStore, ProposalsStore
from utils.store.proposals import Proposal
from utils._funcs import zwsp, log, logToDiscord, emojiName, getChannel, getMessage
from utils.ids import Channels, Roles
from utils.const import SERVER_ID
from time import time
from typing import Tuple, Optional
import re


def setup(BOT):
	BOT.add_cog(Pending(BOT))


async def eraseProposal(proposalMessageID: int, BOT: dc.Client) -> Tuple[bool, Optional[dc.Message]]:
    """Erases the proposal's stored data and pending message. Does not delete the actual proposal message."""
    with ProposalsStore() as proposals:
        if proposalMessageID in proposals.list:
            proposal = proposals.list[proposalMessageID]
            del proposals.list[proposalMessageID]

            if proposal.pending:
                pendingChannel = getChannel(Channels.rulesPending, BOT)
                pendingMessage = await pendingChannel.fetch_message(proposal.pendingID)
                if pendingMessage:
                    await pendingMessage.edit(allowed_mentions=dc.AllowedMentions.none(), content=Pending.pendingMsgMark(pendingMessage.content, _util.EMOTE_CANCELLED, "~~"))
                    return True, pendingMessage
                else:
                    return True, None
    return False, None


class Pending(Cog):


    BASE_MIN_SCORE = 10
    ACTIVE_CHANNELS = ( *RULE_MAKING_CHANNELS, )
    PENDING_CHANNELS = ( Channels.rulesPending, )
    ACCEPTANCE_TIME = (60 * 60 * 24)

    
    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload: dc.RawReactionActionEvent):
        await self.processReaction(payload)

        if payload.channel_id in self.PENDING_CHANNELS and payload.member and payload.member.get_role(Roles.admin):
            message = await getMessage(payload.message_id, payload.channel_id, self.BOT)
            assert self.BOT.user is not None
            if message.author.id != self.BOT.user.id:
                return
            
            emoji = emojiName(payload.emoji)
            if emoji == _util.LOL:
                await message.edit(allowed_mentions=dc.AllowedMentions.none(), content=self.pendingMsgUnmark(message.content))

            elif emoji in ( _util.EMOTE_ACCEPTED, *_util.EMOTES_INSTA_APPROVE, _util.EMOTE_CANCELLED, *_util.EMOTES_VETO ):
                formatting=""
                if emoji in ( _util.EMOTE_ACCEPTED, *_util.EMOTES_INSTA_APPROVE ):
                    formatting="*"
                elif emoji in ( _util.EMOTE_CANCELLED, *_util.EMOTES_VETO ):
                    formatting="~~"
                
                await message.edit(allowed_mentions=dc.AllowedMentions.none(), content=self.pendingMsgMark(message.content, emoji, formatting))
                if emoji in _util.EMOTES_FINALIZED:
                    with ProposalsStore() as proposals:
                        for id, proposal in proposals.list.items():
                            if proposal.pendingID != message.id:
                                continue
                            proposalMessage = await getMessage(id, proposal.channelID, self.BOT)
                            await proposalMessage.remove_reaction(_util.EMOTE_PENDING, member=self.BOT.user)
                            await proposalMessage.add_reaction(emoji)
                            del proposals.list[id]
                            break
            
            await message.remove_reaction(payload.emoji, payload.member)
    

    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, payload: dc.RawReactionActionEvent):
        await self.processReaction(payload)


    async def processReaction(self, payload: dc.RawReactionActionEvent):
        voteEmoji = emojiName(payload.emoji)
        if voteEmoji not in ( *_util.VOTE_REACTIONS, _util.EMOTE_ONEROUND):
            return
        if payload.channel_id not in self.ACTIVE_CHANNELS or (self.BOT.user and payload.user_id == self.BOT.user.id):
            return
        
        message = await getMessage(payload.message_id, payload.channel_id, self.BOT)

        secretScore = 0
        score = 0
        minScore = self.BASE_MIN_SCORE
        for reaction in message.reactions:
            currentEmoji = emojiName(reaction.emoji)
            if currentEmoji in _util.EMOTES_FINALIZED:
                return

            if currentEmoji in _util.VOTE_REACTIONS:
                emojiValue = 0
                if emojiName(reaction.emoji) in _util.PLUS_REACTIONS:
                    emojiValue = 1
                elif emojiName(reaction.emoji) in _util.MINUS_REACTIONS:
                    emojiValue = -1
                if emojiValue == 0:
                    continue

                async for user in reaction.users():
                    if user.bot:
                        continue
                    score += emojiValue
            
            elif currentEmoji == _util.EMOTE_ONEROUND:
                minScore = round(minScore / 2)
        
        # # RULE 049 https://discord.com/channels/1136585230086574170/1136585443958345778/1138925001442086953
        # if payload.channel_id == Channels.itemCrafting:
        #     minScore = 5
        # # RULE 049
        
        # RULE 037 https://discord.com/channels/1136585230086574170/1136585443958345778/1138925001442086953
        if payload.channel_id == Channels.twistMaking:
            minScore = 5
        # RULE 037
        

        with ConfigStore(True) as config:
            with ProposalsStore() as proposals:
                if message.id in proposals.list:
                    proposal = proposals.list[message.id]
                else:
                    proposal = Proposal(content=message.content, channelID=message.channel.id)
                    proposals.list[message.id] = proposal
                
                score += proposal.extraVotes
                secretScore += score
                secretScore += proposal.secretVotes


                enoughScore = score >= minScore
                enoughSecretScore = secretScore >= minScore
                alreadyPassed = False
                if proposal.acceptanceTimestamp > 0 and round(time()) > proposal.acceptanceTimestamp:
                    alreadyPassed = True

                if enoughScore and proposal.visuallyPending and proposal.pendingID > 0:
                    # There already is a pending message. Edit it to reflect the current score
                    pendingChannel = getChannel(Channels.rulesPending, self.BOT)
                    try:
                        pendingMessage = await getMessage(proposal.pendingID, pendingChannel)
                        scoreText = ("+" if score >= 0 else "") + str(score)
                        text = re.sub(
                            r"(Pending proposal .*? with `)(?:.*?)(`.*?)",
                            rf"\1{scoreText}\2",
                            pendingMessage.content
                        )
                        await pendingMessage.edit(allowed_mentions=dc.AllowedMentions.none(), content=text)
                    except dc.NotFound:
                        proposal.pendingID = 0
                        proposal.visuallyPending = False

                if enoughScore and not proposal.visuallyPending:
                    # Proposal enters pending
                    proposal.visuallyPending = True
                    await message.add_reaction(_util.EMOTE_PENDING)

                    pendingChannel = getChannel(Channels.rulesPending, self.BOT)
                    oldMessage = None
                    if proposal.pendingID > 0:
                        oldMessage = await getMessage(proposal.pendingID, pendingChannel)

                    acceptanceTimestamp = round(time()) + self.ACCEPTANCE_TIME
                    messageContent = f'Pending proposal by <@{message.author.id}> - https://discord.com/channels/{SERVER_ID}/{message.channel.id}/{message.id} with `{"+" if score >= 0 else ""}{score}` (can be accepted <t:{acceptanceTimestamp}:R>)\n\n>>> {message.content}'
                    if oldMessage is None:
                        pendingMessage = await pendingChannel.send(allowed_mentions=dc.AllowedMentions.none(), content=messageContent)
                    else:
                        pendingMessage = await pendingChannel.send(allowed_mentions=dc.AllowedMentions.none(), content=messageContent, reference=oldMessage)
                    proposal.pendingID = pendingMessage.id
                
                if enoughSecretScore and not proposal.pending:
                    proposal.pending = True
                    acceptanceTimestamp = round(time()) + 86400
                    proposal.acceptanceTimestamp = acceptanceTimestamp

                    logText = f'https://discord.com/channels/{SERVER_ID}/{message.channel.id}/{message.id} (`{getProposalShorthand(message.content)}`) at {"+" if score >= 0 else "-"}{score} (<@{payload.user_id}>\'s reaction)'
                    if enoughScore:
                        await logToDiscord(self.BOT, 'Pending', logText)
                    else:
                        await logToDiscord(self.BOT, 'Secret Pending', logText + " accepted <t:{acceptanceTimestamp}:R>")

                if not enoughScore and proposal.visuallyPending and not alreadyPassed:
                    # Proposal stops pending
                    proposal.visuallyPending = False
                    assert self.BOT.user is not None
                    await message.remove_reaction(_util.EMOTE_PENDING, member=self.BOT.user)

                    pendingChannel = getChannel(Channels.rulesPending, self.BOT)
                    oldMessage = await pendingChannel.fetch_message(proposal.pendingID)
                    if oldMessage is not None:
                        await oldMessage.edit(allowed_mentions=dc.AllowedMentions.none(), content=self.pendingMsgMark(oldMessage.content, _util.EMOTE_CANCELLED, "~~"))
                    await pendingChannel.send(reference=oldMessage, allowed_mentions=dc.AllowedMentions.none(), content=
                        f'Cancelled proposal https://discord.com/channels/{SERVER_ID}/{message.channel.id}/{message.id} by <@{message.author.id}> at <t:{round(time())}:f> (`{getProposalShorthand(message.content)}`)'
                    )

                if not enoughSecretScore and proposal.pending and not alreadyPassed:
                    proposal.pending = False
                    proposal.acceptanceTimestamp = 0

                    logText = f'https://discord.com/channels/{SERVER_ID}/{message.channel.id}/{message.id} (`{getProposalShorthand(message.content)}`) at {"+" if score >= 0 else "-"}{score} (<@{payload.user_id}>\'s unreaction)'
                    if not enoughScore:
                        await logToDiscord(self.BOT, 'Cancelled', logText)
                    else:
                        await logToDiscord(self.BOT, 'Secret Cancelled', logText)


                if config.logAllVotes:
                    key = "Vote "
                    if voteEmoji in _util.PLUS_REACTIONS:
                        key += "Added"
                    elif voteEmoji in _util.MINUS_REACTIONS:
                        key += "Removed"
                    await logToDiscord(self.BOT, key, f'https://discord.com/channels/1136585230086574170/{message.channel.id}/{message.id} - {voteEmoji} by <@{payload.user_id}> - now at `{secretScore}` votes')
    
    @classmethod
    def pendingMsgMark(cls, text: str, emote: str, formatting: str = ""):
        if len(emote) > 15:
            emote = f'<:{emote}>'
        s = f'{formatting}{emote} {text}{formatting}'
        return s
    
    @classmethod
    def pendingMsgUnmark(cls, text: str):
        s = text.strip("*_~").strip()
        s = s[s.index(" ") + 1:]
        return s


    @commands.Cog.listener()
    async def on_raw_message_delete(self, payload: dc.RawMessageDeleteEvent):
        if payload.channel_id not in self.ACTIVE_CHANNELS or payload.message_id < 1137018933971521556:
            return
        
        proposalWasPending, oldMessage = await eraseProposal(payload.message_id, self.BOT)

        if proposalWasPending:
            if payload.cached_message:
                messageContent = f'Cancelled proposal `{getProposalShorthand(payload.cached_message.content)}` by <@{payload.cached_message.author.id}> due to message deletion at <t:{round(time())}:f>'
            else:
                messageContent = f'Cancelled proposal `{payload.message_id}` due to message deletion at <t:{round(time())}:f>'
            pendingChannel = getChannel(Channels.rulesPending, self.BOT)
            if oldMessage:
                await pendingChannel.send(reference=oldMessage, allowed_mentions=dc.AllowedMentions.none(), content=messageContent)
            else:
                await pendingChannel.send(allowed_mentions=dc.AllowedMentions.none(), content=messageContent)
