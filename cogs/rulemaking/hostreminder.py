from typing import List, Tuple
import discord as dc
from discord.ext import tasks, commands
from utils.cogs import Cog
from utils.stores import ConfigStore, ProposalsStore
from utils.store.proposals import Proposal
from utils._funcs import log, logToDiscord, getChannel
from utils.ids import Channels
from utils.const import SERVER_ID, OWNER_ID
from time import time


def setup(BOT):
	BOT.add_cog(HostReminder(BOT))

class HostReminder(Cog):

    @commands.Cog.listener()
    async def on_ready(self):
        self.every_5_min.start()

    @tasks.loop(minutes=5)
    async def every_5_min(self):
        with ConfigStore(True) as config:
            with ProposalsStore() as proposals:
                proposalsListed: List[Tuple[int, Proposal]] = []
                nextProposalTime = 99999999
                for messageID, proposal in sorted(proposals.list.items(), key=lambda i: i[1].acceptanceTimestamp):
                    if proposal.acceptanceTimestamp == 0 or proposal.remindered:
                        continue
                    if proposal.acceptanceTimestamp < round(time()):
                        proposalsListed.append((messageID, proposal))
                    else:
                        nextProposalTime = proposal.acceptanceTimestamp
                        break
                
                if len(proposalsListed) > 0 and (round(time()) - proposalsListed[0][1].acceptanceTimestamp > 1800 or not (nextProposalTime - round(time()) <= 1800)):
                    s = ""
                    for messageID, proposal in proposalsListed:
                        s += f'On <t:{proposal.acceptanceTimestamp}:f>: https://discord.com/channels/{SERVER_ID}/{Channels.rulesPending}/{proposal.pendingID}\n'
                        proposals.list[messageID].remindered = True
                    s += f'Rule acceptance reminder <@{OWNER_ID}> (<t:{round(time())}:t>)'
                    hostCommands = getChannel(Channels.hostCommands, self.BOT)
                    await hostCommands.send(content=s)
