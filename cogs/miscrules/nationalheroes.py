import discord as dc
from discord.utils import get
from discord.ext import bridge, commands, tasks
from utils.const import SERVER_ID
from utils.stores import MiscStore
from utils.cogs import Cog
from utils.ids import Roles
from utils._funcs import getRole, getGuild
from utils import error_handling
from time import time


def setup(BOT):
	BOT.add_cog(NationalHeroes(BOT))


# RULE 015 https://discord.com/channels/1136585230086574170/1136585443958345778/1137501776984162476
class NationalHeroes(Cog):

    ROLE = Roles.nationalHero

    @bridge.bridge_command()
    @commands.check(error_handling.is_admin)
    async def nationalhero(self, ctx: bridge.context.BridgeExtContext, member: dc.Member, days: float):
        await member.add_roles(getRole(self.ROLE, ctx.guild))
        with MiscStore() as miscStore:

            if member.id in miscStore.nationalHeroTimestamps:
                currentTime = miscStore.nationalHeroTimestamps[member.id]
            else:
                currentTime = round(time())
            endingTime = currentTime + round(86400 * days)
            miscStore.nationalHeroTimestamps[member.id] = endingTime

            if days == round(days):
                days = int(days)
            await ctx.reply(allowed_mentions=dc.AllowedMentions.none(), content=f":medal: Gave <@{member.id}> the National Hero role for `{days}` days, expiring <t:{endingTime}:R>! :medal:", delete_after=5)
            if ctx.message:
                await ctx.message.delete()


    @commands.Cog.listener()
    async def on_ready(self):
        self.every_5_min.start()

    @tasks.loop(minutes=5)
    async def every_5_min(self):
        with MiscStore() as miscStore:
            for memberID, timestamp in sorted(miscStore.nationalHeroTimestamps.items(), key=lambda i: i[1]):
                if timestamp <= round(time()):
                    del miscStore.nationalHeroTimestamps[memberID]
                    member = get(self.BOT.get_all_members(), id=memberID)
                    if member:
                        await member.remove_roles(getRole(self.ROLE, getGuild(SERVER_ID, self.BOT)))
                else:
                    break
