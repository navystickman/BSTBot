import discord as dc
from discord.ext import bridge, commands
from utils.stores import PlayersStore
from utils.cogs import Cog
from utils._funcs import logToDiscord
from random import random


def setup(BOT):
	BOT.add_cog(NerdEmoji(BOT))


# RULE 036 https://discord.com/channels/1136585230086574170/1136585443958345778/1138395206568390747
class NerdEmoji(Cog):

    @commands.Cog.listener()
    async def on_message(self, message: dc.Message):
        if random() < (1 / 1000):
            await message.add_reaction("🤓")
    
    @bridge.bridge_command()
    @dc.guild_only()
    async def freekromer1997guaranteednoscam(self, ctx: bridge.BridgeExtContext):
        # If you are the first person to ever run this command you are entitled to 50 coins. For every person after you that runs this command expecting to get money you'll also get 3 coins
        await logToDiscord(self.BOT, "Free Kromer", f"<@450030739476709378> holy shit someone used free kromer (specifically <@{ctx.author.id}>)", allowPings=True)
        await ctx.respond("***💸💸💸You have found the secret FREE KROMER command Holy shit 💸💸💸💸***")
