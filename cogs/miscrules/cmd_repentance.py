import discord as dc
from discord.utils import get
from discord.ext import bridge, commands
from utils.cogs import Cog
from utils.ids import Roles
from utils._funcs import getRole
from utils import error_handling
from random import random


def setup(BOT):
	BOT.add_cog(CMDRepentance(BOT))


# RULE 034 https://discord.com/channels/1136585230086574170/1136585443958345778/1138152532250726430
class CMDRepentance(Cog):

    ROLE = Roles.shameRole
    PERCENTAGE = 0.69

    @bridge.bridge_command()
    @commands.check(error_handling.in_bot_commands)
    @commands.check(error_handling.has_shame_role)
    async def repentance(self, ctx: bridge.context.BridgeExtContext):
        success = random() < (self.PERCENTAGE / 100)
        if success:
            assert isinstance(ctx.author, dc.Member)
            await ctx.author.remove_roles(getRole(self.ROLE, ctx.guild))
            await ctx.reply(f":tada: <@{ctx.author.id}> has successfully gotten rid of their Shame Role! :tada:")
        else:
            ping = random() < 0.1
            allowed_mentions = dc.AllowedMentions.none()
            if ping:
                allowed_mentions = dc.AllowedMentions.all()
            await ctx.reply(f"Command failed. Try again for a {self.PERCENTAGE}% chance to get rid of <@&1137124095507562577>!", allowed_mentions=allowed_mentions)
