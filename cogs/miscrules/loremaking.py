import discord as dc
from discord.ext import commands
from utils.cogs import Cog
from utils.stores import MiscStore
from utils._funcs import logToDiscord, emojiName, getChannel, getMessage
from utils.ids import Channels


def setup(BOT):
	BOT.add_cog(LoreMaking(BOT))


class LoreMaking(Cog):

    MIN_SCORE_FOR_LORE = 7
    VOTE_REACTIONS = ('👍', '👎')

    @commands.Cog.listener()
    async def on_message(self, message: dc.Message):
        if message.channel.id != Channels.loreMaking or message.author.bot:
            return
        try:
            for emote in self.VOTE_REACTIONS:
                await message.add_reaction(emote)
        except dc.errors.NotFound:
            pass
        except dc.errors.Forbidden:
            self.BOT.last_deleted_message = message.id
            await message.delete()
            await message.channel.send(content=f"<@{message.author.id}> Unblock Imposter to use <#{message.channel.id}>!", delete_after=7)
    
    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload: dc.RawReactionActionEvent):
        await self.processReaction(payload)

    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, payload: dc.RawReactionActionEvent):
        await self.processReaction(payload)

    async def processReaction(self, payload: dc.RawReactionActionEvent):
        voteEmoji = emojiName(payload.emoji)
        if voteEmoji not in self.VOTE_REACTIONS or payload.channel_id != Channels.loreMaking or (self.BOT.user and payload.user_id == self.BOT.user.id):
            return
        
        message = await getMessage(payload.message_id, payload.channel_id, self.BOT)

        score = 0
        minScore = self.MIN_SCORE_FOR_LORE
        for reaction in message.reactions:
            currentEmoji = emojiName(reaction.emoji)
            if currentEmoji == '☑️':
                return
            async for user in reaction.users():
                if user.bot:
                    continue
                if currentEmoji == '👍':
                    score += 1
                elif currentEmoji == '👎':
                    score -= 1
        
        if score >= minScore:
            with MiscStore() as miscStore:
                await message.add_reaction('☑️')
                await getChannel(Channels.officialLore, self.BOT).send(allowed_mentions=dc.AllowedMentions.none(), content=f"{str(miscStore.loreIndex).zfill(3)} (by <@{message.author.id}>):\n>>> {message.content}")
                miscStore.loreIndex += 1
