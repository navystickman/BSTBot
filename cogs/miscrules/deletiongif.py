import discord as dc
from discord.utils import get
from discord.ext import commands
from utils.cogs import Cog
from utils._funcs import getChannel
from utils.ids import Channels
from cogs.rulemaking.noediting import NoEditing


def setup(BOT):
	BOT.add_cog(DeletionGif(BOT))


# RULE 091 https://discord.com/channels/1136585230086574170/1136585443958345778/1141124305422385172
class DeletionGif(Cog):

    ACTIVE_CHANNELS = ( Channels.ruleMaking, Channels.twistMaking, Channels.itemCrafting, Channels.loreMaking )
    NO_EDIT_CHANNELS = NoEditing.ACTIVE_CHANNELS
    TARGET_CHANNEL = Channels.ruleDiscussion
    GIF_URL = "https://tenor.com/view/dbz-discord-gif-24306382"

    @commands.Cog.listener()
    async def on_raw_message_delete(self, payload: dc.RawMessageDeleteEvent):
        if payload.channel_id not in self.ACTIVE_CHANNELS or self.BOT.last_deleted_message == payload.message_id or payload.cached_message is None or payload.cached_message.author.bot:
            return
        
        if payload.channel_id in self.NO_EDIT_CHANNELS and payload.cached_message.edited_at is not None:
            return
        
        content = f"<@{payload.cached_message.author.id}> {self.GIF_URL}"
        await getChannel(self.TARGET_CHANNEL, self.BOT).send(content)
