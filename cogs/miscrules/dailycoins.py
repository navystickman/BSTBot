import discord as dc
from discord.ext import commands, tasks
from utils.const import NEWDAYTIME, SERVER_ID
from utils.store.players import Player
from utils.stores import PlayersStore
from utils.cogs import Cog


def setup(BOT):
	BOT.add_cog(DailyCoins(BOT))


def howManyCoins(player: Player):
    return 1


# RULE 029 https://discord.com/channels/1136585230086574170/1136585443958345778/1137802162089508945
class DailyCoins(Cog):

    @commands.Cog.listener()
    async def on_ready(self):
        self.every_day.start()

    @tasks.loop(time=NEWDAYTIME)
    async def every_day(self):
        with PlayersStore() as players:
            for tag, player in players.playerDict.items():
                if not player.alive:
                    continue
                coins = howManyCoins(player)
                player.raw_balance += coins
                await player.updateBalance(self.BOT, f"daily coins, 029")
