import discord as dc
from discord.ext import bridge, commands
from utils.cogs import Cog
from utils._funcs import emojiName, getMessage


def setup(BOT):
	BOT.add_cog(PinReact(BOT))


# RULE 084 https://discord.com/channels/1136585230086574170/1136585443958345778/1140704432322007100
class PinReact(Cog):

    REACTIONS_FOR_PIN = 5

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload: dc.RawReactionActionEvent):
        voteEmoji = emojiName(payload.emoji)
        if voteEmoji != "📌":
            return
        
        message = await getMessage(payload.message_id, payload.channel_id, self.BOT)
        if message.pinned:
            return
        
        for reaction in message.reactions:
            emoji = emojiName(reaction.emoji)
            if emoji != "📌":
                continue
            
            reactions = 0
            async for user in reaction.users():
                reactions += 1
            
            if reactions >= self.REACTIONS_FOR_PIN:
                await message.pin()
                break
