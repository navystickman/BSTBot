from typing import Any, Optional
import discord as dc
from discord.ext import bridge, commands
from utils.cogs import Cog
from utils.stores import PlayersStore
from utils.store.players import Player, PlayerNotFoundError
from utils._funcs import log, logToDiscord, getGuild, threadify
from utils.const import SERVER_ID
from utils.ids import Channels
from utils import error_handling
from pydantic.error_wrappers import ErrorWrapper, ValidationError
from re import sub


def setup(BOT):
    BOT.add_cog(CMDPlayers(BOT))

class CMDPlayers(Cog):

    @bridge.bridge_group(invoke_without_command=True)
    @dc.guild_only()
    @commands.check(error_handling.is_admin)
    async def player(self, ctx: bridge.BridgeExtContext):
        await error_handling.availableSubcommands(ctx, self.player)

    @player.command()
    async def list(self, ctx: bridge.BridgeExtContext):
        with PlayersStore() as players:
            response = ''
            for tag, player in players.playerDict.items():
                response += f'> <@{player.userID}> `{tag}`\n'
            if response == "":
                response = "Couldn't find any members lol"
            await ctx.respond(allowed_mentions=dc.AllowedMentions.none(), content=response)

    @player.command()
    @dc.guild_only()
    async def get(self, ctx: bridge.BridgeExtContext, player: str):
        with PlayersStore() as players:
            playerObj = await players.get(player, ctx)
            await ctx.respond(allowed_mentions=dc.AllowedMentions.none(), content=f'Player {playerObj}: `{playerObj}`')

    @player.command()
    @dc.guild_only()
    async def add(self, ctx: bridge.BridgeExtContext, player: str, balance: float = 0, alive: bool = True):
        newPlayer = None
        with PlayersStore() as players:
            try:
                playerObj = await players.get(player, ctx)
                await ctx.respond(allowed_mentions=dc.AllowedMentions.none(), content=f'{playerObj} is already registered as a player with tag `{playerObj.tag}`!')
                return
            except PlayerNotFoundError:
                pass
            
            if isinstance(player, dc.Member):
                id = player.id
                name = player.display_name
            else:
                id = 0
                name = str(player)
            
            newPlayer = Player(
                userID=id,
                tag=self.makeTagFromName(name),
                name=name,
                alive=alive,
                raw_balance=balance
            )
            players.playerDict[newPlayer.tag] = newPlayer
            if id > 0:
                await self.addLogChannel(newPlayer)
            threadify(newPlayer.addToSheet)
            
        assert isinstance(newPlayer, Player)
        await ctx.respond(allowed_mentions=dc.AllowedMentions.none(), content=f'Added {newPlayer} as a {"contestant" if alive else "spectator"} (tag: `{newPlayer.tag}`, name: `{newPlayer.name}`, balance: `{newPlayer.balanceStr()}`)')

    @player.command()
    @dc.guild_only()
    async def remove(self, ctx: bridge.BridgeExtContext, player: str):
        with PlayersStore() as players:
            playerObj = await players.get(player, ctx)
            await ctx.respond(allowed_mentions=dc.AllowedMentions.none(), content=f'Removed player {playerObj} (`{playerObj.name}`)!')
            for key, value in players.playerDict.items():
                if value is playerObj:
                    del players.playerDict[key]
                    return
            await ctx.respond(allowed_mentions=dc.AllowedMentions.none(), content=f'Uhhity ohhity failed to remove')

    @player.command()
    @dc.guild_only()
    async def makechannel(self, ctx: bridge.BridgeExtContext, player: str):
        with PlayersStore() as players:
            playerObj = await players.get(player, ctx)
            if playerObj.userID == 0:
                await ctx.respond(f":skull: Player `{playerObj.tag}` doesn't have assigned user ID!")
                return
            channel = await self.addLogChannel(playerObj)
            await ctx.respond(f"Added channel for `{playerObj.tag}`! (<#{channel.id}>)")

    @player.command()
    @dc.guild_only()
    async def edit(self, ctx: bridge.BridgeExtContext, player: str, key: str, value):
        with PlayersStore() as players:
            playerObj = await players.get(player, ctx)
            
            validateAssignment = playerObj.Config.validate_assignment
            playerObj.Config.validate_assignment = True
            try:
                if key == "balance":
                    playerObj.raw_balance = value
                    await playerObj.updateBalance(self.BOT, f"adamanti changed your balance")
                    await logToDiscord(self.BOT, "Set Balance", f'Set {playerObj}\'s coins to {value}')
                else:
                    setattr(playerObj, key, value)
                    threadify(playerObj.updateAttrOnSheet, key)
            except ValidationError as e:
                assert isinstance(e.raw_errors[0], ErrorWrapper)
                await ctx.respond(allowed_mentions=dc.AllowedMentions.none(), content=f'Pydantic couldn\'t set field `{key}`: `{e.raw_errors[0].exc}`')
                playerObj.Config.validate_assignment = validateAssignment
                return
            playerObj.Config.validate_assignment = validateAssignment
            await ctx.respond(allowed_mentions=dc.AllowedMentions.none(), content=f'Set field `{key}` for player {player} to `{getattr(playerObj, key)}`')
    
    def makeTagFromName(self, name: str):
        return sub("[^a-zA-Z]", "", name.lower())
    
    async def addLogChannel(self, player: Player):
        category = self.BOT.get_channel(Channels.CategoryBotLog)
        assert isinstance(category, dc.CategoryChannel)
        guild = getGuild(SERVER_ID, self.BOT)
        newChannel = await category.create_text_channel(player.name, topic=f"Hi <@{id}> this is your private channel where the bot will tell you your balance", overwrites={
            guild.default_role: dc.PermissionOverwrite(read_messages=False),
            guild.get_member(player.userID): dc.PermissionOverwrite(read_messages=True),
        })
        player.logChannelID = newChannel.id
        return newChannel
