import discord as dc
from discord.ext import bridge, commands
from utils.cogs import Cog
from utils._funcs import log, logToDiscord
from utils import error_handling
from os.path import isfile
from os import walk
from sys import exit


def setup(BOT):
    BOT.add_cog(CMDDev(BOT))

class CMDDev(Cog):

    @bridge.bridge_command()
    @commands.check(error_handling.is_admin)
    async def talk(self, ctx: bridge.BridgeExtContext, channel: dc.TextChannel, message: str):
        await channel.send(message)

    @bridge.bridge_command()
    @commands.is_owner()
    async def restart(self, ctx: bridge.BridgeExtContext):
        await ctx.respond("Restarting bot")
        exit(0)

    @bridge.bridge_command()
    @commands.is_owner()
    async def kill(self, ctx: bridge.BridgeExtContext):
        await ctx.respond("Killed bot :( (he was not the imposter)")
        exit(8)

    @bridge.bridge_command()
    @commands.check(error_handling.is_admin)
    async def listcogs(self, ctx: bridge.BridgeExtContext):
        response = ' '.join(map(lambda k: f'`{k}`', self.BOT.cogs.keys()))
        await ctx.respond(response)

    @bridge.bridge_command()
    @commands.check(error_handling.is_admin)
    async def reloadcog(self, ctx: bridge.BridgeExtContext, cogname: str):
        await self._reloadCogCmd(ctx, cogname)

    @bridge.bridge_command()
    @commands.check(error_handling.is_admin)
    async def loadcog(self, ctx: bridge.BridgeExtContext, cogname: str):
        await self._reloadCogCmd(ctx, cogname)
    
    async def _reloadCogCmd(self, ctx: bridge.BridgeExtContext, cogname: str):
        if cogname[-3:] != ".py":
            cogname += ".py"
        path = f"cogs/{cogname}"
        if not isfile(path):
            foundCogs = []
            for root, dirs, files in walk("cogs"):
                for fileName in files:
                    if fileName == cogname:
                        foundCogs.append(root + "/" + fileName)
        else:
            foundCogs = [path]
        
        if len(foundCogs) > 1:
            await ctx.respond(f":skull: There are multiple cogs named `{cogname}`. Be more specific pls")
            return
        elif len(foundCogs) == 0:
            await ctx.respond(f":skull: Could not find any cogs named `{cogname}`. Womp womp")
            return
        else:
            cogPath = foundCogs[0]
            cogPath = cogPath[:-3].replace('/', '.')
            try:
                self.BOT.unload_extension(cogPath)
            except dc.errors.ExtensionNotLoaded:
                pass
            self.BOT.load_extension(cogPath)
            await ctx.respond(f":white_check_mark: Successfully loaded cog `{cogname}`")
