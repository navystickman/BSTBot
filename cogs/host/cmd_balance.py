from typing import Any, Optional, Union
import discord as dc
from discord.ext import bridge, commands
from utils.cogs import Cog
from utils._funcs import log, logToDiscord
from utils.stores import PlayersStore
from utils.store.players import Player, PlayerNotFoundError
from utils import error_handling


def setup(BOT):
	BOT.add_cog(CMDBalance(BOT))

class CMDBalance(Cog):

    @bridge.bridge_group(invoke_without_command=True)
    @dc.guild_only()
    @bridge.map_to("bal")
    async def bal(self, ctx: bridge.BridgeExtContext):
        await self.getCommand(ctx)

    @bal.command()
    @dc.guild_only()
    async def get(self, ctx: bridge.BridgeExtContext, player: Optional[str] = None):
        await self.getCommand(ctx, player)
    
    async def getCommand(self, ctx: bridge.BridgeExtContext, player: Optional[str] = None):
        you = True
        query = player
        if query is None:
            assert isinstance(ctx.author, dc.Member)
            query = ctx.author
        elif await error_handling.is_admin(ctx):
            you = False
        
        with PlayersStore() as players:
            if you:
                errorMessage = "You do not have a coin balance!"
            else:
                errorMessage = None

            if isinstance(query, dc.Member):
                playerObj = await players.get(query, errorMessage=errorMessage)
            else:
                playerObj = await players.get(query, ctx, errorMessage=errorMessage)
            
            if you:
                await ctx.respond(allowed_mentions=dc.AllowedMentions.none(), content=f'You have `{playerObj.balanceStr()}` coins!')
            else:
                await ctx.respond(allowed_mentions=dc.AllowedMentions.none(), content=f'The player {playerObj} has `{playerObj.balanceStr()}` coins!')
    
    @bal.command()
    @dc.guild_only()
    @commands.check(error_handling.is_admin)
    async def add(self, ctx: bridge.BridgeExtContext, player: str, value: float, reason: str):
        await self.changeBalanceCommand(ctx, player, value, reason=reason, log="Added Coins")
    
    @bal.command()
    @dc.guild_only()
    @commands.check(error_handling.is_admin)
    async def take(self, ctx: bridge.BridgeExtContext, player: str, value: float, reason: str):
        await self.changeBalanceCommand(ctx, player, -value, reason=reason, log="Took Coins")
    
    @bal.command()
    @dc.guild_only()
    @commands.check(error_handling.is_admin)
    async def set(self, ctx: bridge.BridgeExtContext, player: str, value: float, reason: str):
        await self.changeBalanceCommand(ctx, player, value, False, reason=reason, log="Set Coins")
    
    async def changeBalanceCommand(self, ctx: bridge.BridgeExtContext, player: str, value: float, additive: bool = True, reason: str = "", log: str = ""):
        with PlayersStore() as players:
            playerObj = await players.get(player, ctx)
            prevBal = playerObj.balanceStr()
            if additive:
                playerObj.raw_balance += value
            else:
                playerObj.raw_balance = value
            
            await playerObj.updateBalance(self.BOT, reason)

            await ctx.respond(allowed_mentions=dc.AllowedMentions.none(), content=f'Player {playerObj} went from `{prevBal}` to `{playerObj.balanceStr()}` coins.')
            await logToDiscord(self.BOT, log, f'Gave {value} coins to {playerObj}')
    
    @bal.command()
    @dc.guild_only()
    @commands.check(error_handling.is_admin)
    async def buy(self, ctx: bridge.BridgeExtContext, player: str, rawprice: float, itemname: str):
        try:
            await error_handling.bal_purchase(ctx, rawprice, itemname, player=player)
            await ctx.respond(allowed_mentions=dc.AllowedMentions.none(), content=f'Purchase successful!')
        except:
            await ctx.respond(allowed_mentions=dc.AllowedMentions.none(), content=f'Purchase failed!')
    
    @bal.command()
    @dc.guild_only()
    @commands.check(error_handling.is_admin)
    async def special(self, ctx: bridge.BridgeExtContext, player: str, format: str):
        with PlayersStore() as players:
            playerObj = await players.get(player, ctx)
            prevBal = playerObj.balanceStr()
            if format == "":
                format = "{}"
            playerObj.balanceFormat = format
            await ctx.respond(allowed_mentions=dc.AllowedMentions.none(), content=f'Player {playerObj} went from `{prevBal}` to `{playerObj.balanceStr()}` coins.')
    
    @bal.command()
    @dc.guild_only()
    @commands.check(error_handling.is_admin)
    async def giveall(self, ctx: bridge.BridgeExtContext, value: float, reason: str = "adamanti gave everyone money"):
        with PlayersStore() as players:
            for tag, player in players.playerDict.items():
                if not player.alive:
                    continue
                player.raw_balance += value
                await player.updateBalance(self.BOT, reason)
        await ctx.respond(allowed_mentions=dc.AllowedMentions.none(), content=f'Gave every signed up player `{value}` coins.')
    
    @bal.command()
    @dc.guild_only()
    @commands.check(error_handling.is_admin)
    async def takeall(self, ctx: bridge.BridgeExtContext, value: float, reason: str = "adamanti took coins from everyone"):
        with PlayersStore() as players:
            for tag, player in players.playerDict.items():
                if not player.alive:
                    continue
                player.raw_balance -= value
                await player.updateBalance(self.BOT, reason)
        await ctx.respond(allowed_mentions=dc.AllowedMentions.none(), content=f'Took `{value}` coins away from every signed up player.')
    
    @bal.command()
    @dc.guild_only()
    @commands.check(error_handling.is_admin)
    async def royalties(self, ctx: bridge.BridgeExtContext, player: str, votes: int, subject: str = ""):
        if subject != "":
            subject += " "
        reason = subject + "Royalties"
        value = votes / 2
        await self.changeBalanceCommand(ctx, player, value, reason=reason, log="Gave Royalties")
