import discord as dc
from discord.ext import commands
from utils.cogs import Cog
from utils.ids import Channels
from utils._funcs import logToDiscord
from utils import error_handling


def setup(BOT):
	BOT.add_cog(ErrorHandling(BOT))


class ErrorHandling(Cog):

    @commands.Cog.listener()
    async def on_command_error(self, ctx: dc.ApplicationContext, error: dc.DiscordException):
        if isinstance(error, commands.errors.NotOwner):
            await ctx.respond(f':skull: Only the owner can run `{ctx.command.name}` silly')
        elif isinstance(error, error_handling.NotAdmin):
            await ctx.respond(f':skull: Only an admin can run `{ctx.command.name}` silly')
        elif isinstance(error, error_handling.NotAlive):
            await ctx.respond(f':skull: You have to be an alive contestant to use `{ctx.command.name}` sorry')
        elif isinstance(error, error_handling.NotContestant):
            await ctx.respond(f':skull: You have to be a contestant to use `{ctx.command.name}` sorry')
        elif isinstance(error, error_handling.NotShameRole):
            await ctx.respond(f':skull: You dont have the shame role why are you running `{ctx.command.name}` lol')
        elif isinstance(error, error_handling.NotInBotCommands):
            await ctx.respond(f':skull: Go to <#{Channels.botCommands}> silly', delete_after=5)
        elif isinstance(error, error_handling.NotInBotCommandsOrDMs):
            await ctx.respond(f':skull: Go to <#{Channels.botCommands}> (or the bot\'s dms i guess) silly', delete_after=5)
        elif isinstance(error, commands.errors.MissingRequiredArgument):
            await ctx.respond(f':skull: You forgot to specify `{error.param}`')
        elif isinstance(error, commands.CommandInvokeError):
            
            e = error.original
            if isinstance(e, error_handling.NotEnoughBalance):
                await ctx.respond(f':skull: You need {round(e.neededBalance, 10)} coins to buy {e.itemName} (you have {e.currentBalance}{"" if e.rawBalance is None else f" (which is basically {round(e.rawBalance, 10)})"})')
            else:
                await ctx.respond(f':skull: {str(e)[:500]}')
        
        elif isinstance(error, commands.CommandNotFound):
            await ctx.respond(f':skull: Unknown command :(')
        else:
            raise error
