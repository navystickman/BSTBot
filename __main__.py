import discord as dc
from utils.const import BOT, TOKEN
from utils._funcs import log
import os


@BOT.event
async def on_ready():
    log("Bot logged in")

    if not os.path.isdir("store"):
        os.mkdir("store")

    await BOT.change_presence(activity=dc.Activity(type=dc.ActivityType.watching, name="from inside that sussy vent"))


cogsLoaded = []

for root, _, files in os.walk("cogs"):
    for filename in files:
        if not filename.endswith('py') or filename.startswith('_'):
            continue
        
        filepath = root.replace("/", ".") + "." + filename[:-3]
        BOT.load_extension(filepath)

        cogsLoaded.append(filename[:-3])

log(f"Loaded cogs: {', '.join(cogsLoaded)}")


BOT.run(TOKEN)
