import discord as dc
from discord.ext import bridge, commands
from utils._funcs import logToDiscord
from utils.ids import Channels, Roles
from typing import TYPE_CHECKING, Callable, Optional, Tuple, Union
from utils.store.players import Player, PlayersStore, PlayerList, PlayerNotFoundError


class NotEnoughBalance(dc.DiscordException):
    neededBalance: float
    currentBalance: str
    rawBalance: Optional[float]
    itemName: Optional[str]
    def __init__(self, neededBalance: float, currentBalance: str, rawBalance: Optional[float], itemName: Optional[str], *args):
        super().__init__(None, *args)
        self.neededBalance = neededBalance
        self.currentBalance = currentBalance
        self.rawBalance = rawBalance
        self.itemName = itemName


def _purchaseComparison(needed: float, player: Player, ctx) -> Tuple[bool, float, float]:
    # Funny game logic applied by rules goes here
    have = player.balance

    discount = 1
    needed *= discount

    return have >= needed, have, needed

async def _make_bal_predicate(bal: float, checker, store: Optional[PlayerList] = None, readonly: Optional[bool] = True, itemName: Optional[str] = None):
    async def predicate(ctx) -> bool:
        player = None
        if store is None and readonly is None:
            raise Exception("specify one of these lmao")
        if store is None and readonly is not None:
            with PlayersStore(readonly) as players:
                enough, player = await checker(ctx, bal, players)
                if enough:
                    return True
            assert isinstance(player, Player)
        else:
            enough, player = await checker(ctx, bal, store)
            if enough:
                return True
        
        rawBalance = None
        if player.balance != player.raw_balance:
            rawBalance = player.balance
        raise NotEnoughBalance(bal, player.balanceStr(), rawBalance, itemName)
    
    return predicate

async def bal_needed(ctx, bal: float, itemName: Optional[str], store: Optional[PlayerList] = None, player: Union[Player, str, dc.Member, None] = None):
    async def _checkBalance(ctx, bal: float, store: PlayerList) -> Tuple[bool, Player]:

        nonlocal player
        if player is None:
            player = await store.get(ctx.author)
        elif not isinstance(player, Player):
            player = await store.get(player, ctx)
        result, _, _ = _purchaseComparison(bal, player, ctx)
        return result, player

    predicate = await _make_bal_predicate(bal, _checkBalance, store, itemName=itemName)
    return await predicate(ctx)

async def bal_purchase(ctx, bal: float, itemName: Optional[str], store: Optional[PlayerList] = None, player: Union[Player, str, dc.Member, None] = None):
    async def _deductBalance(ctx, bal: float, store: PlayerList) -> Tuple[bool, Player]:

        nonlocal player
        if player is None:
            player = await store.get(ctx.author)
        elif not isinstance(player, Player):
            player = await store.get(player, ctx)
        result, have, needed = _purchaseComparison(bal, player, ctx)

        if result:
            player.raw_balance -= needed
            store.host.raw_balance += needed
            await player.updateBalance(ctx.bot, f'Purchased "{itemName}"')
            await store.host.updateBalance(ctx.bot, f'Purchased "{itemName}"')
            await logToDiscord(ctx.bot, "Purchase", f'<@{player.userID}> purchased `{itemName}` for `{needed}` (raw price: `{bal}`)')
        return result, player

    predicate = await _make_bal_predicate(bal, _deductBalance, store, readonly=False, itemName=itemName)
    return await predicate(ctx)


class NotAdmin(commands.CheckFailure):
    pass

async def is_admin(ctx):
    if ctx.author.get_role(Roles.admin):
        return True
    raise NotAdmin()


class NotAlive(commands.CheckFailure):
    pass

async def is_alive(ctx):
    if ctx.author.get_role(Roles.contestant):
        return True
    raise NotAlive()


class NotContestant(commands.CheckFailure):
    pass

async def is_contestant(ctx):
    if is_alive(ctx) or ctx.author.get_role(Roles.contestantDead):
        return True
    raise NotContestant()


class NotShameRole(commands.CheckFailure):
    pass

async def has_shame_role(ctx):
    if ctx.author.get_role(Roles.shameRole):
        return True
    raise NotShameRole()


class NotInBotCommands(commands.CheckFailure):
    pass

async def in_bot_commands(ctx: commands.Context):
    if not isinstance(ctx.author, dc.Member):
        raise NotInBotCommands()
    if ctx.author.get_role(Roles.admin) or ctx.channel.id == Channels.botCommands:
        return True
    raise NotInBotCommands()


class NotInBotCommandsOrDMs(commands.CheckFailure):
    pass

async def in_bot_commands_or_dms(ctx: commands.Context):
    if ctx.guild is None:
        return True
    return await in_bot_commands(ctx)


def _defaultFormat(s: bridge.core.BridgeCommand) -> str:
    return f'`{s.name}`'

async def availableSubcommands(ctx: bridge.BridgeExtContext, group: bridge.BridgeCommandGroup, message: str = ":skull: Available subcommands:", separator: str = " ", subcommandFormat: Callable[[bridge.core.BridgeCommand], str] = _defaultFormat):
    subcommands = []
    subcommand: bridge.core.BridgeCommand
    for subcommand in group.walk_commands():
        if subcommand.parent == group:
            subcommands.append(subcommandFormat(subcommand))
    await ctx.respond(message + separator + separator.join(subcommands))
