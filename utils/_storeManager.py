from io import TextIOWrapper
from json import loads
from os.path import isfile
from utils._funcs import log
from pydantic import BaseModel
from pydantic.error_wrappers import ValidationError
from typing import Any, Type


ID = int

class StoreManager:
    PATH: str = ""
    STORE_MODEL: Type[BaseModel]

    readonly: bool
    store: BaseModel
    _file: TextIOWrapper
    _preExisted: bool

    def __init__(self, readonly=False):
        hasPath = self.PATH != ""
        hasStoreModel = hasattr(self, "STORE_MODEL")
        if not hasPath or not hasStoreModel:
            if not hasPath and hasStoreModel:
                raise NotImplementedError(f"JSONStore cannot be instantiated without PATH")
            elif hasPath and not hasStoreModel:
                raise NotImplementedError(f"JSONStore cannot be instantiated without STORE_MODEL")
            elif not hasPath and not hasStoreModel:
                raise NotImplementedError(f"JSONStore cannot be instantiated without PATH and STORE_MODEL")
        
        self.readonly = readonly

    def __enter__(self) -> Any:
        filepath = "store/" + self.PATH + ".json"
        self._preExisted = True
        if not isfile(filepath):
            self._preExisted = False
        
        if self.readonly and self._preExisted:
            self._file = open(filepath, "r")
        elif self._preExisted:
            self._file = open(filepath, "r+")
        else:
            self._file = open(filepath, "w+")
        content = self._file.read()

        if content == "":
            jsonData = {}
        else:
            jsonData = loads(content)
        
        if not isinstance(jsonData, dict):
            raise TypeError(f'Store file "{self.PATH}.json" is not an object')
        
        try:
            self.store = self.STORE_MODEL.parse_obj(jsonData)
        except ValidationError as e:
            log(f'ERROR: Failed to parse json from "{self.PATH}.json" into model {self.STORE_MODEL.__name__}!')
            raise e
        return self.store
    
    def __exit__(self, exc_type, exc_value, exc_traceback) -> bool:
        if not self.readonly or not self._preExisted:
            content = self.store.json(sort_keys=True, indent=4)
            self._file.seek(0)
            self._file.truncate()
            self._file.write(content)
        self._file.close()
        if exc_type is not None:
            return False
        return True
