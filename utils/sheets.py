from pathlib import Path
import gspread
from functools import lru_cache

SHEET_URL = "https://docs.google.com/spreadsheets/d/1nGZhcC8yLtQWn3Tac9YfW06uRmANupK87mqB3G30P64"

@lru_cache(maxsize=None)
def sheet(url: str) -> gspread.Spreadsheet:
    key = url[url.find('/spreadsheets/d/') + 16:]
    if '/' in key:
        key = key[:key.find('/')]

    gc = gspread.service_account(filename=Path("./gspread_service_account.json"))
    return gc.open_by_key(key)

@lru_cache(maxsize=None)
def worksheet(sheetURL: str, worksheetName: str) -> gspread.Worksheet:
    return sheet(sheetURL).worksheet(worksheetName)

class SheetWorksheets:
    CONTESTANTS = "Contestants"

class ContestantsCols:
    TAG = 1
    NAME = 2
    USERID = 3
    BOOKURL = 5
    BALANCE = 6
    ALIVE = 7
