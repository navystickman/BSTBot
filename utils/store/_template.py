from utils._storeManager import StoreManager, ID
from pydantic import BaseModel
from typing import List, Dict


class Model(BaseModel):
    __root__: None

class Store(StoreManager):
    PATH = ""
    STORE_MODEL = Model
    def __enter__(self) -> Model:
        return super().__enter__()
