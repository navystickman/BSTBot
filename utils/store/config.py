from utils._storeManager import StoreManager, ID
from pydantic import BaseModel
from typing import List, Dict


class Config(BaseModel):
    logAllVotes: bool = False

class ConfigStore(StoreManager):
    PATH = "config"
    STORE_MODEL = Config
    def __enter__(self) -> Config:
        return super().__enter__()
