from utils._storeManager import StoreManager, ID
from pydantic import BaseModel
from typing import List, Dict


class MiscStores(BaseModel):
    reminderChance: float = 1
    loreIndex: int = 0
    rulesMadeToday: Dict[ID, int] = {}
    nationalHeroTimestamps: Dict[ID, int] = {}
    onlyOneMessage: List[ID] = []

class MiscStore(StoreManager):
    PATH = "misc"
    STORE_MODEL = MiscStores
    def __enter__(self) -> MiscStores:
        return super().__enter__()
