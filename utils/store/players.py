import discord as dc
from discord.ext.commands import CommandInvokeError
import gspread
from utils._storeManager import StoreManager, ID
from utils._funcs import getChannel, threadify
from utils.sheets import worksheet, SHEET_URL, SheetWorksheets, ContestantsCols
from pydantic import BaseModel
from typing import List, Dict, Optional, Union, overload
from discord.ext.commands import MemberConverter, Context
from discord import Member, DiscordException
from math import floor


class PlayerNotFoundError(DiscordException):
    pass


class Player(BaseModel):
    tag: str = ""
    name: str = ""
    userID: ID = 0
    logChannelID: ID = 0
    alive: bool = False
    raw_balance: float = 0.0
    balanceFormat: str = "{}"


    def __init__(__pydantic_self__, **data) -> None:
        super().__init__(**data)
        lastRawBalance[__pydantic_self__.tag] = __pydantic_self__.raw_balance


    def balanceStr(self) -> str:
        bal = round(self.raw_balance, 10)
        if round(bal) == bal:
            bal = round(bal)
        s = "{:,}".format(bal)
        if len(str(round(bal-floor(bal), 10))) == (2 + 10):
            s = s[:-1] + "…"
        s = self.balanceFormat.format(s)
        return s
    

    @property
    def balance(self) -> float:
        return getBalanceFromPlayer(self)
    

    async def updateBalance(self, client: dc.Client, reason: str = ""):
        lastBalance = lastRawBalance[self.tag]
        self.raw_balance = round(self.raw_balance, 10)
        if self.raw_balance == lastBalance:
            return
        
        threadify(self.updateOnSheet, ContestantsCols.BALANCE)
        
        if self.logChannelID > 0:
            channel = getChannel(self.logChannelID, client)
            CURRENCY = "coins"
            if reason != "":
                reason = f" ({reason})"
            
            diff = self.raw_balance - lastBalance
            if diff < 0:
                diff = -diff
            diff = round(diff, 10)
            if round(diff) == diff:
                diff = round(diff)
            
            if self.raw_balance > lastBalance:
                await channel.send(f":coin: Added `{diff}` to your balance{reason}. You now have `{self.balanceStr()}` {CURRENCY}")
            else:
                await channel.send(f":coin: Spent `{diff}` from your balance{reason}. You now have `{self.balanceStr()}` {CURRENCY}")
        
        lastRawBalance[self.tag] = self.raw_balance
    

    def __str__(self) -> str:
        if self.userID > 0:
            return f'<@{self.userID}>'
        else:
            return f'`{self.name}`'

    def updateAttrOnSheet(self, attr):
        if attr == "tag":
            self.updateOnSheet(ContestantsCols.TAG)
        elif attr == "name":
            self.updateOnSheet(ContestantsCols.NAME)
        elif attr == "userID":
            self.updateOnSheet(ContestantsCols.USERID)
        elif attr == "raw_balance":
            self.updateOnSheet(ContestantsCols.BALANCE)
        elif attr == "alive":
            self.updateOnSheet(ContestantsCols.ALIVE)

    def updateOnSheet(self, col):
        row = self.getSheetRow()
        ws = worksheet(SHEET_URL, SheetWorksheets.CONTESTANTS)
        if col == ContestantsCols.TAG:
            ws.update_cell(row, col, self.tag)
        elif col == ContestantsCols.NAME:
            ws.update_cell(row, col, self.name)
        elif col == ContestantsCols.USERID:
            ws.update_cell(row, col, str(self.userID))
        elif col == ContestantsCols.BOOKURL:
            raise CommandInvokeError(Exception("idk how to do that"))
        elif col == ContestantsCols.BALANCE:
            ws.update_cell(row, col, self.balanceStr())
        elif col == ContestantsCols.ALIVE:
            ws.update_cell(row, col, self.alive)
        else:
            raise CommandInvokeError(Exception(f"Unknown column on contestant sheet: `{col}`"))

    def getSheetRow(self):
        if self.tag not in sheetRows:
            ws = worksheet(SHEET_URL, SheetWorksheets.CONTESTANTS)
            rowCell = ws.find(self.tag, in_column=1)
            if rowCell is None:
                raise CommandInvokeError(Exception(f"Player `{self.tag}` not found in sheet"))
            else:
                sheetRows[self.tag] = rowCell.row
        return sheetRows[self.tag]

    def addToSheet(self):
        ws = worksheet(SHEET_URL, SheetWorksheets.CONTESTANTS)
        if ws.find(self.tag, in_column=1) is not None:
            raise CommandInvokeError(Exception(f"Player `{self.tag}` already in sheet"))
        colValues = ws.col_values(1)
        try:
            firstEmptyRow = colValues.index("")
            colValues = colValues[:firstEmptyRow]
        except:
            pass
        firstEmptyRow = len(colValues) + 1
        sheetRows[self.tag] = firstEmptyRow
        self.updateOnSheet(ContestantsCols.TAG)
        self.updateOnSheet(ContestantsCols.NAME)
        self.updateOnSheet(ContestantsCols.USERID)
        self.updateOnSheet(ContestantsCols.BALANCE)
        self.updateOnSheet(ContestantsCols.ALIVE)

lastRawBalance: Dict[str, float] = {}
sheetRows: Dict[str, int] = {}


def getBalanceFromPlayer(player: Player):
    return player.raw_balance


class PlayerList(BaseModel):
    playerDict: Dict[str, Player] = {}

    @property
    def host(self):
        return self.playerDict["adamanti"]

    @overload
    async def get(self, query: Member, ctx: None = None, errorMessage: Optional[str] = None) -> Player: ...

    @overload
    async def get(self, query: str, ctx: Context, errorMessage: Optional[str] = None) -> Player: ...

    async def get(self, query: Union[str, Member], ctx: Optional[Context] = None, errorMessage: Optional[str] = None) -> Player:
        if isinstance(query, str):
            if query in self.playerDict:
                return self.playerDict[query]
            for player in self.playerDict.values():
                if player.name.lower() == query.lower():
                    return player
        
        member = None
        if isinstance(query, Member):
            member = query
        elif ctx:
            try:
                member = await MemberConverter().convert(ctx, query)
            except:
                # Query isn't a member
                pass
        if member:
            for player in self.playerDict.values():
                if player.userID == member.id:
                    return player
        
        if errorMessage is None:
            if member:
                errorMessage = f"Couldn't find player <@{member.id}> in player list!"
            else:
                errorMessage = f"Couldn't find player `{query}` in player list!"
        raise PlayerNotFoundError(errorMessage)


class PlayersStore(StoreManager):
    PATH = "players"
    STORE_MODEL = PlayerList
    def __enter__(self) -> PlayerList:
        return super().__enter__()
