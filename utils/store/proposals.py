from utils._storeManager import StoreManager, ID
from pydantic import BaseModel
from typing import List, Dict


class Proposal(BaseModel):
    content: str = ""
    pending: bool = False
    pendingID: ID = 0
    channelID: ID = 0
    acceptanceTimestamp: int = 0
    extraVotes: int = 0
    secretVotes: int = 0
    visuallyPending: bool = False
    remindered: bool = False

class ProposalDict(BaseModel):
    list: Dict[ID, Proposal] = {}

class ProposalsStore(StoreManager):
    PATH = "proposals"
    STORE_MODEL = ProposalDict
    def __enter__(self) -> ProposalDict:
        return super().__enter__()

