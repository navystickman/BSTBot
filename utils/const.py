import discord as dc
from discord.ext import bridge
from dotenv import load_dotenv
from os import getenv
from time import time
import datetime

class BSTBot(bridge.Bot):
    last_deleted_message: int
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.last_deleted_message = 0

OWNER_ID = 450030739476709378
SERVER_ID = 1136585230086574170
PREFIX = "-"

load_dotenv()
TOKEN = getenv("BOT_TOKEN")
INTENTS = dc.Intents.all()

BOT = BSTBot(command_prefix=PREFIX, intents=INTENTS)
BOT.remove_command('help')

STARTUP = time()

NEWDAYTIME = datetime.time(hour=4, tzinfo=datetime.timezone.utc)
