import discord as dc
from discord.ext import commands
from discord.utils import get
from utils.ids import Channels
from datetime import datetime
from typing import Optional, Union, overload
import threading

zwsp = "​"

def log(s: str):
    print(f'[{datetime.now()}] {s}')

async def logToDiscord(client: dc.Client, key: str, s: str = "", allowPings: bool = False):
    await getChannel(Channels.logs, client).send(
        content=f'<t:{round(datetime.now().timestamp())}:F> `[{key}]`' + ("" if s == "" else " ") + s,
        allowed_mentions=dc.AllowedMentions.none() if not allowPings else dc.AllowedMentions.all(),
    )

def emojiName(emoji: Union[dc.PartialEmoji, dc.Emoji, str]):
    if isinstance(emoji, str):
        return emoji
    if emoji.id is not None:
        return f'{emoji.name}:{emoji.id}'
    else:
        return emoji.name

def getGuild(guildID: int, client: dc.Client) -> dc.Guild:
    guild = client.get_guild(guildID)
    assert guild is not None
    return guild


def getChannel(channelID: int, client: dc.Client) -> dc.abc.Messageable:
    channel = client.get_channel(channelID)
    assert isinstance(channel, dc.abc.Messageable)
    return channel


@overload
async def getMessage(messageID: int, channel: dc.abc.Messageable) -> dc.Message: ...
@overload
async def getMessage(messageID: int, channel: int, client: dc.Client) -> dc.Message: ...

async def getMessage(messageID: int, channel: Union[dc.abc.Messageable, int], client: Optional[dc.Client] = None) -> dc.Message:
    if isinstance(channel, int):
        assert client is not None
        channel = getChannel(channel, client)
    
    message = await channel.fetch_message(messageID)
    assert message is not None
    return message


def getRole(roleID: int, guild: Optional[dc.Guild]) -> dc.Role:
    assert guild is not None
    role = get(guild.roles, id=roleID)
    assert role is not None
    return role


def threadify(func, *args):
    thread = threading.Thread(target=func, args=args)
    thread.daemon = True
    thread.start()
    return thread